package com.wismay.gc.entity;

import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotBlank;

//JPA标识
@Entity
@Table(name = "t_project")
public class Project extends IdEntity {

	private String name;
	private String description;

	// JSR303 BeanValidator的校验规则
	@NotBlank
	public String getDescription() {
		return description;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
