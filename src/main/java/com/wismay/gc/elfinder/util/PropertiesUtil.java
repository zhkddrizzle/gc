package com.wismay.gc.elfinder.util;

import java.io.IOException;
import java.util.Properties;

public class PropertiesUtil {

	private static Properties prop = new Properties();  
	
    static{
		
        
        try {  
            prop.load(PropertiesUtil.class.getClassLoader().getResourceAsStream("application.properties"));  
              
        } catch(IOException e) {  
            e.printStackTrace();  
        }  
	}
    
    public static String getProperty(String key){
    	return prop.getProperty(key);
    }

}
