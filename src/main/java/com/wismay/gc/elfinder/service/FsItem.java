package com.wismay.gc.elfinder.service;

public interface FsItem {
	FsVolume getVolume();
}