package com.wismay.gc.core.transform;

import java.sql.Types;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

public class JavaNameing {
	/**
	 * 数据库的数据类型映射为Java数据类型
	 * 
	 * @param dataType
	 * @return String Java数据类型的名称
	 */
	public static String transformType(int dataType) {
		String typeStr = "String";
		switch (dataType) {
		case Types.BIGINT:
			typeStr = "Long";
			break;
		case Types.BOOLEAN:
			typeStr = "Boolean";
			break;
		case Types.DATE:
			typeStr = "Date";
			break;
		case Types.TIME:
			typeStr = "Date";
			break;
		case Types.TIMESTAMP:
			typeStr = "Date";
			break;
		case Types.DECIMAL:
			typeStr = "Double";
			break;
		case Types.DOUBLE:
			typeStr = "Double";
			break;
		case Types.FLOAT:
			typeStr = "Float";
			break;
		case Types.INTEGER:
			typeStr = "Integer";
			break;

		default:
			typeStr = "String";
			break;
		}
		return typeStr;
	}

	/**
	 * 转换命名风格 base_comm_code_item ==>> BaseCommCodeItem
	 * 
	 * @param db_name
	 * @param nameType
	 * @param removeStrs
	 *            需要移除的字符串
	 * @return
	 */
	public static String dbNameToObjName(String db_name, NameType type, List<String> removeStrs) {
		if (removeStrs != null) {
			for (String str : removeStrs) {
				db_name = db_name.replaceFirst(str, "");
			}
		}

		// _name_name -->> name_name
		if (db_name.startsWith("_")) {
			db_name = db_name.replaceFirst("_", "");
		}

		String[] tnames = db_name.split("_");

		String objName = "";

		for (int i = 0; i < tnames.length; i++) {
			String str = tnames[i];
			// 如果是驼峰命名，不将第一个单词的首字母转为大写
			if (str != null && NameType.CAMEL.equals(type) && i == 0) {
				objName += str.toLowerCase();
			} else if (str != null && str.length() > 1) {
				String first = str.substring(0, 1);
				objName += first.toUpperCase() + str.substring(1).toLowerCase();
			}// 单词只有一个字母，首字母不转为大写
			else if (str != null && str.length() == 1) {
				objName += str.toLowerCase();
			}
		}
		return objName;
	}

	/**
	 * 将第一个字母转为大写
	 * 
	 * @param str
	 * @return
	 */
	public static String toCamel(String str) {
		String result = "";
		if (StringUtils.isNotBlank(str)) {
			String first = str.substring(0, 1);
			result = first.toUpperCase() + str.substring(1).toLowerCase();
		}
		return result;
	}

}
