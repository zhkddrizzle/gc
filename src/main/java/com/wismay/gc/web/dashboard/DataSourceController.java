package com.wismay.gc.web.dashboard;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springside.modules.mapper.JsonMapper;

import com.wismay.gc.entity.DataSource;
import com.wismay.gc.service.dashboard.DataSourceService;

/**
 * 
 * @author Peter
 */
@Controller
@RequestMapping(value = "/data-source")
public class DataSourceController {

	private static Logger logger = LoggerFactory.getLogger(DataSourceController.class);
	private static JsonMapper jsonMapper = JsonMapper.nonDefaultMapper();

	@Autowired
	private DataSourceService dataSourceService;

	@RequestMapping(value = "ajax/detail/{projectId}", method = RequestMethod.GET)
	public String detail(@PathVariable("projectId") Long projectId, Model model) {
		logger.info("GET:/data-source/ajax/detail/");
		DataSource dataSource = dataSourceService.findByProjectId(projectId);
		model.addAttribute("dataSource", dataSource);
		return "dashboard/data-source-detail";
	}

	@RequestMapping(value = "ajax/save-update-form/{projectId}", method = RequestMethod.GET)
	public String saveOrUpdateForm(@PathVariable("projectId") Long projectId, Model model) {
		logger.info("GET:/data-source/save-update-form/" + projectId);
		DataSource dataSource = dataSourceService.findByProjectId(projectId);
		if (dataSource != null) {
			model.addAttribute("dataSource", dataSource);
		} else {
			model.addAttribute("dataSource", new DataSource(projectId));
		}
		return "dashboard/data-source-form";
	}

	@RequestMapping(value = "ajax/save-update", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public String saveOrUpdate(@Valid DataSource dataSource, RedirectAttributes redirectAttributes) {
		logger.info("GET:/data-source/ajax/save-update");
		dataSourceService.save(dataSource);
		Map<String, Object> message = new HashMap<String, Object>();
		message.put("result", true);
		message.put("message", "操作成功!");
		return jsonMapper.toJson(message);
	}

	@RequestMapping(value = "ajax/upload-dirver-file/{projectId}", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public String uploadDirverFile(@PathVariable("projectId") Long projectId, @RequestParam MultipartFile driverFile, Model model, HttpServletRequest request) throws Exception {
		logger.info("GET:/data-source/ajax/upload-dirver-file");
		// MultipartFile driverFile = dataSource.getDriverFile();
		logger.info("ContentType:" + driverFile.getContentType());
		logger.info("Name:" + driverFile.getName());
		logger.info("OriginalFilename:" + driverFile.getOriginalFilename());
		logger.info("Size:" + driverFile.getSize());

		String ctxDir = request.getSession().getServletContext().getRealPath("");
		logger.info("ctxDir:" + ctxDir);

		dataSourceService.saveDriverFile(projectId, driverFile.getOriginalFilename(), ctxDir, driverFile);

		Map<String, Object> message = new HashMap<String, Object>();
		message.put("result", true);
		message.put("message", "操作成功!");
		return jsonMapper.toJson(message);
	}

	@RequestMapping(value = "ajax/test-db-connect/{projectId}", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public String testDbConnect(@PathVariable("projectId") Long projectId, HttpServletRequest request) {
		logger.info("GET:/data-source/ajax/test-db-connect");
		Map<String, Object> message = new HashMap<String, Object>();
		try {
			String ctxDir = request.getSession().getServletContext().getRealPath("");
			dataSourceService.testMysqlConnect(projectId, ctxDir);
			message.put("result", true);
			message.put("message", "操作成功!");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			message.put("result", false);
			message.put("message", e.getMessage());
		}

		return jsonMapper.toJson(message);
	}
	
	@RequestMapping(value = "ajax/gen-code/{projectId}", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public String genCode(@PathVariable("projectId") Long projectId, HttpServletRequest request) {
		logger.info("GET:/data-source/ajax/gen-code/"+projectId);
		Map<String, Object> message = new HashMap<String, Object>();
		try {
			String ctxDir = request.getSession().getServletContext().getRealPath("");
			dataSourceService.genCode(projectId, ctxDir);
			message.put("result", true);
			message.put("message", "操作成功!");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			message.put("result", false);
			message.put("message", e.getMessage());
		}

		return jsonMapper.toJson(message);
	}

}
