package com.wismay.gc.web.dashboard;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletRequest;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springside.modules.mapper.JsonMapper;

import com.wismay.gc.entity.Project;
import com.wismay.gc.service.dashboard.ProjectService;

/**
 * 
 * @author Peter
 */
@Controller
@RequestMapping(value = "/project")
public class ProjectController {

	private static Logger logger = LoggerFactory.getLogger(ProjectController.class);
	private static JsonMapper jsonMapper = JsonMapper.nonDefaultMapper();

	@Autowired
	private ProjectService projectService;

	@RequestMapping(value = "myProject/{projectId}", method = RequestMethod.GET)
	public String myProject(@PathVariable("projectId") Long projectId, Model model) {
		logger.info("GET:project/myProject");
		model.addAttribute("projectId", projectId);
		return "dashboard/my-project";
	}

	@RequestMapping(method = RequestMethod.GET)
	public String list(Model model, ServletRequest request) {
		List<Project> projects = projectService.getAllProject();
		model.addAttribute("projects", projects);
		return "dashboard/project";
	}

	@RequestMapping(value = "create", method = RequestMethod.GET)
	public String createForm(Model model) {
		logger.info("GET:project/create");
		model.addAttribute("project", new Project());
		model.addAttribute("action", "create");
		return "dashboard/project-form";
	}

	@RequestMapping(value = "create", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public String create(@Valid Project newProject, RedirectAttributes redirectAttributes) {
		projectService.saveProject(newProject);
		Map<String, Object> message = new HashMap<String, Object>();
		message.put("result", true);
		message.put("message", "操作成功!");
		return jsonMapper.toJson(message);
	}

	@RequestMapping(value = "update/{id}", method = RequestMethod.GET)
	public String updateForm(@PathVariable("id") Long id, Model model) {
		model.addAttribute("project", projectService.getProject(id));
		model.addAttribute("action", "update");
		return "dashboard/project-form";
	}

	@RequestMapping(value = "update", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public String update(@Valid @ModelAttribute("project") Project project, RedirectAttributes redirectAttributes) {
		projectService.saveProject(project);
		Map<String, Object> message = new HashMap<String, Object>();
		message.put("result", true);
		message.put("message", "操作成功!");
		return jsonMapper.toJson(message);
	}

	@RequestMapping(value = "delete/{id}")
	public String delete(@PathVariable("id") Long id, RedirectAttributes redirectAttributes) {
		projectService.deleteProject(id);
		redirectAttributes.addFlashAttribute("message", "操作成功");
		return "redirect:/project/";
	}

}
