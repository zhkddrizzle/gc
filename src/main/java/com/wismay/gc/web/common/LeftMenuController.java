package com.wismay.gc.web.common;

import java.util.List;

import javax.servlet.ServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.wismay.gc.entity.Project;
import com.wismay.gc.service.dashboard.ProjectService;

/**
 * 
 * @author Peter
 */
@Controller
@RequestMapping(value = "/ajax/left-menu")
public class LeftMenuController {

	private static Logger logger = LoggerFactory.getLogger(LeftMenuController.class);

	@Autowired
	private ProjectService projectService;

	@RequestMapping(method = RequestMethod.GET)
	public String list(Model model, ServletRequest request) {
		logger.info("GET:left-menu");
		List<Project> projects = projectService.getAllProject();
		model.addAttribute("projects", projects);
		return "common/left-menu";
	}

}
