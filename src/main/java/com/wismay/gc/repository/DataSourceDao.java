package com.wismay.gc.repository;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.wismay.gc.entity.DataSource;

public interface DataSourceDao extends PagingAndSortingRepository<DataSource, Long>, JpaSpecificationExecutor<DataSource> {

	DataSource findByProjectId(Long projectId);
}
