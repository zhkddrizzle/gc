<%@ page contentType="text/html;charset=UTF-8" %>
<%@ page import="org.apache.shiro.web.filter.authc.FormAuthenticationFilter"%>
<%@ page import="org.apache.shiro.authc.ExcessiveAttemptsException"%>
<%@ page import="org.apache.shiro.authc.IncorrectCredentialsException"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>

<html>
<head>
	<head>
	<title></title>
	<style type="text/css">
		body .modal-data-mode-info {
		    /* new custom width */
		    width: 950px;
		    /* must be half of the width, minus scrollbar on the left (30px) */
   			 margin-left: -475px;
		}
	</style>
	<script type="text/javascript">
	   //ajax加载左侧菜单
		$.ajax({
			url: "${ctx}/ajax/left-menu",
		    cache: true
		}).done(function( html ) {
		   $("#left-menu").html(html);
		});
	   
	   function genCode(projectId){
		   $.ajax({
				url: "${ctx}/data-source/ajax/gen-code/"+projectId,
			    cache: true
			}).done(function( data ) {
				var html='';
				if(data.result){
					html="<div class='alert alert-success'>"+
						"<button type='button' class='close' data-dismiss='alert'>×</button>"+
						"<strong>代码生成成功！</strong> "+
					"</div> ";
				}else{
					html="<div class='alert alert-error'>"+
						"<button type='button' class='close' data-dismiss='alert'>×</button>"+
						"<strong>代码生成失败！<br/>"+data.message+"</strong>"+
					"</div>";
				}
				
			   $("#genCodeResult").html(html);
			   
			});
	   }
	   
		//ajax加载DataSource detail
		function showDataSourceDetail(){
			$.ajax({
				url: "${ctx}/data-source/ajax/detail/${projectId}",
			    cache: true
			}).done(function( html ) {
			   $("#data-source-detail").html(html);
			});
		}
		
		$(document).ready(function() {
			showDataSourceDetail();
		});
		
		
		/***   fancyBox  ***/
		$(document).ready(function() {
			//初始化fancyBox
			$("a#openSaveOrUpdateProjectBox").fancybox({
			});
		});

		//触发：配置数据源PickerBox
		function openSaveOrUpdateProjectBox(){
			$("a#openSaveOrUpdateProjectBox").trigger('click');
		}
		
		
		
	</script>	
	
	<script type="text/javascript" charset="utf-8">
		$().ready(function() {
			var elf = $('#elfinder_${projectId}').elfinder({
				//debug      : true,
				//root       : 'd:\\gc\\template\\${projectId}',
				customData : {projectId:'${projectId}'},
				lang       : 'zh_CN',             // language (OPTIONAL)
				url        : '${ctx}/elfinder/connector'  // connector URL (REQUIRED)
			}).elfinder('instance');			
		});
	</script>

</head>
</head>

<body>
<a id="openSaveOrUpdateProjectBox" data-fancybox-type="iframe" href="${ctx}/data-source/ajax/save-update-form/${projectId}"></a>
<div class="container-fluid">
	<div class="row-fluid">
			
		<!-- left menu starts -->
		<div class="span2 main-menu-span">
			<div class="well nav-collapse sidebar-nav" id="left-menu">
				
			</div><!--/.well -->
		</div><!--/span-->
		<!-- left menu ends -->
		
		<noscript>
			<div class="alert alert-block span10">
				<h4 class="alert-heading">Warning!</h4>
				<p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a> enabled to use this site.</p>
			</div>
		</noscript>
		
		<div id="content" class="span10">
		<!-- content starts -->
		

		<div>
			<ul class="breadcrumb">
				<li>
					<a href="${ctx}/">Home</a> <span class="divider">/</span>
				</li>
				<li>
					<a href="#">${projectName}</a>
				</li>
			</ul>
		</div>
		
		
		<div class="row-fluid">
			<div class="box span12">
				<div class="box-header well">
					<h2><i class="icon-info-sign"></i> 数据源配置</h2>
					<div class="box-icon">
						<a href="javascript:openSaveOrUpdateProjectBox();" class="btn btn-setting btn-round"><i class="icon-cog"></i></a>
						<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
					</div>
				</div>
				<div class="box-content">
					<div id="data-source-detail"></div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
		<div class="row-fluid">
			<div class="box span12">
				<div class="box-header well">
					<h2><i class="icon-info-sign"></i> 代码模板</h2>
					<div class="box-icon">
						<a href="#" class="btn btn-setting btn-round btn-dataModeExampleInfo"><i class="icon-exclamation-sign"></i></a>
						<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
						<a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
					</div>
				</div>
				<div class="box-content">
					<!-- Element where elFinder will be created (REQUIRED) -->
					<div id="elfinder_${projectId}"></div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
		<!-- content ends -->
		
		<div class="center">
		<a href="javascript:genCode(${projectId});" class="btn btn-large">生成代码</a>
		<div id="genCodeResult"></div>
		
	</div>
		</div><!--/#content.span10-->
	</div><!--/fluid-row-->
			

<script type="text/javascript">
	$(function(){
		
		$('.btn-dataModeExampleInfo').click(function(e){
			e.preventDefault();
			$('#dataModeExample').modal('show');
		});
		
	});
</script>
	<div class="modal-data-mode-info modal hide fade" id="dataModeExample">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">×</button>
			<h3>freemarker数据模型(Data-Mode)</h3>
		</div>
		<div class="modal-body">
			<jsp:include page="../common/data-mode.jsp"></jsp:include>
		</div>
		<div class="modal-footer">
			<a href="#" class="btn" data-dismiss="modal">关闭</a>
		</div>
	</div>

	
</div><!--/.fluid-container-->

</body>
</html>
