<%@ page contentType="text/html;charset=UTF-8" %>
<%@ page import="org.apache.shiro.web.filter.authc.FormAuthenticationFilter"%>
<%@ page import="org.apache.shiro.authc.ExcessiveAttemptsException"%>
<%@ page import="org.apache.shiro.authc.IncorrectCredentialsException"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>

<html>
<head>
	<head>
	<title></title>
	<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

<!-- The styles -->
	<link id="bs-css" href="${ctx}/static/charisma-master/css/bootstrap-cerulean.css" rel="stylesheet">
	<style type="text/css">
	    body {
		    padding-bottom: 40px;
	    }
	    .sidebar-nav {
		    padding: 9px 0;
	    }
	    /* footer */
	    #footer {
			margin-top: 15px;
			padding: 15px 0px 0px 0px;
			font-size: 95%;
			text-align: center;
			/*border-top: 2px solid #cccccc;*/
	    }
		
	    #footer a {color: #999;}
	</style>
	<link href="${ctx}/static/charisma-master/css/bootstrap-responsive.css" rel="stylesheet" />
	<link href="${ctx}/static/charisma-master/css/charisma-app.css" rel="stylesheet" />
	<link href="${ctx}/static/charisma-master/css/jquery-ui-1.8.21.custom.css" rel="stylesheet" />
	<link href='${ctx}/static/charisma-master/css/fullcalendar.css' rel='stylesheet' />
	<link href='${ctx}/static/charisma-master/css/fullcalendar.print.css' rel='stylesheet'  media='print' />
	<link href='${ctx}/static/charisma-master/css/chosen.css' rel='stylesheet' />
	<link href='${ctx}/static/charisma-master/css/uniform.default.css' rel='stylesheet' />
	<link href='${ctx}/static/charisma-master/css/colorbox.css' rel='stylesheet' />
	<link href='${ctx}/static/charisma-master/css/jquery.cleditor.css' rel='stylesheet' />
	<link href='${ctx}/static/charisma-master/css/jquery.noty.css' rel='stylesheet' />
	<link href='${ctx}/static/charisma-master/css/noty_theme_default.css' rel='stylesheet' />
	<link href='${ctx}/static/charisma-master/css/elfinder.min.css' rel='stylesheet' />
	<link href='${ctx}/static/charisma-master/css/elfinder.theme.css' rel='stylesheet' />
	<link href='${ctx}/static/charisma-master/css/jquery.iphone.toggle.css' rel='stylesheet' />
	<link href='${ctx}/static/charisma-master/css/opa-icons.css' rel='stylesheet' />
	<link href='${ctx}/static/charisma-master/css/uploadify.css' rel='stylesheet' />

	<!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
	  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

	<!-- The fav icon -->
	<link rel="shortcut icon" href="${ctx}/static/charisma-master/img/favicon.ico" />

	<!-- external javascript
	================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<!-- jQuery -->
	<script src="${ctx}/static/charisma-master/js/jquery-1.7.2.min.js"></script>
	<!-- jQuery UI -->
	<script src="${ctx}/static/charisma-master/js/jquery-ui-1.8.21.custom.min.js"></script>
	<!-- jQuery Validation -->
	<script src="${ctx}/static/jquery-validation/1.11.1/jquery.validate.min.js" type="text/javascript"></script>
	<script src="${ctx}/static/jquery-validation/1.11.1/messages_bs_zh.js" type="text/javascript"></script>
	<link href="${ctx}/static/jquery-validation/1.11.1/validate.css" rel="stylesheet">
	
	<!-- transition / effect library -->
	<script src="${ctx}/static/charisma-master/js/bootstrap-transition.js"></script>
	<!-- alert enhancer library -->
	<script src="${ctx}/static/charisma-master/js/bootstrap-alert.js"></script>
	<!-- modal / dialog library -->
	<script src="${ctx}/static/charisma-master/js/bootstrap-modal.js"></script>
	<!-- custom dropdown library -->
	<script src="${ctx}/static/charisma-master/js/bootstrap-dropdown.js"></script>
	<!-- scrolspy library -->
	<script src="${ctx}/static/charisma-master/js/bootstrap-scrollspy.js"></script>
	<!-- library for creating tabs -->
	<script src="${ctx}/static/charisma-master/js/bootstrap-tab.js"></script>
	<!-- library for advanced tooltip -->
	<script src="${ctx}/static/charisma-master/js/bootstrap-tooltip.js"></script>
	<!-- popover effect library -->
	<script src="${ctx}/static/charisma-master/js/bootstrap-popover.js"></script>
	<!-- button enhancer library -->
	<script src="${ctx}/static/charisma-master/js/bootstrap-button.js"></script>
	<!-- accordion library (optional, not used in demo) -->
	<script src="${ctx}/static/charisma-master/js/bootstrap-collapse.js"></script>
	<!-- carousel slideshow library (optional, not used in demo) -->
	<script src="${ctx}/static/charisma-master/js/bootstrap-carousel.js"></script>
	<!-- autocomplete library -->
	<script src="${ctx}/static/charisma-master/js/bootstrap-typeahead.js"></script>
	<!-- tour library -->
	<script src="${ctx}/static/charisma-master/js/bootstrap-tour.js"></script>
	<!-- library for cookie management-->
	<script src="${ctx}/static/charisma-master/js/jquery.cookie.js"></script>
	<!-- calander plugin -->
	<script src='${ctx}/static/charisma-master/js/fullcalendar.min.js'></script>
	<!-- data table plugin -->
	<script src='${ctx}/static/charisma-master/js/jquery.dataTables.min.js'></script>

	<!-- chart libraries start -->
	<script src="${ctx}/static/charisma-master/js/excanvas.js"></script>
	<script src="${ctx}/static/charisma-master/js/jquery.flot.min.js"></script>
	<script src="${ctx}/static/charisma-master/js/jquery.flot.pie.min.js"></script>
	<script src="${ctx}/static/charisma-master/js/jquery.flot.stack.js"></script>
	<script src="${ctx}/static/charisma-master/js/jquery.flot.resize.min.js"></script>
	<!-- chart libraries end -->

	<!-- select or dropdown enhancer -->
	<script src="${ctx}/static/charisma-master/js/jquery.chosen.min.js"></script>
	<!-- checkbox, radio, and file input styler -->
	<script src="${ctx}/static/charisma-master/js/jquery.uniform.min.js"></script>
	<!-- plugin for gallery image view -->
	<script src="${ctx}/static/charisma-master/js/jquery.colorbox.min.js"></script>
	<!-- rich text editor library -->
	<script src="${ctx}/static/charisma-master/js/jquery.cleditor.min.js"></script>
	<!-- notification plugin -->
	<script src="${ctx}/static/charisma-master/js/jquery.noty.js"></script>
	<!-- file manager library -->
	<script src="${ctx}/static/charisma-master/js/jquery.elfinder.min.js"></script>
	<!-- star rating plugin -->
	<script src="${ctx}/static/charisma-master/js/jquery.raty.min.js"></script>
	<!-- for iOS style toggle switch -->
	<script src="${ctx}/static/charisma-master/js/jquery.iphone.toggle.js"></script>
	<!-- autogrowing textarea plugin -->
	<script src="${ctx}/static/charisma-master/js/jquery.autogrow-textarea.js"></script>
	<!-- multiple file upload plugin -->
	<script src="${ctx}/static/charisma-master/js/jquery.uploadify-3.1.min.js"></script>
	<!-- history.js for cross-browser state change on ajax -->
	<script src="${ctx}/static/charisma-master/js/jquery.history.js"></script>
	<!-- application script for Charisma demo -->
	<script src="${ctx}/static/charisma-master/js/charisma.js"></script>
	<!-- fancyBox -->
	<link rel="stylesheet" href="${ctx}/static/fancyBox/2.1.2/source/jquery.fancybox.css?v=2.1.0" type="text/css" media="screen" />
	<script type="text/javascript" src="${ctx}/static/fancyBox/2.1.2/source/jquery.fancybox.pack.js?v=2.1.0"></script>
	
	<!-- jquery form -->
	<script type="text/javascript" src="${ctx}/static/jquery-form/jquery.form.min.js"></script>
	<!-- jquery blockUI -->
	<script type="text/javascript" src="${ctx}/static/jquery-blockUI/jquery.blockUI.js"></script>
	
	
	<script>
		// prepare the form when the DOM is ready 
		$(document).ready(function() { 
		    // bind form using 'ajaxForm' 
		    $('#myForm').ajaxForm({ 
		        target:        '#myResult',   // target element(s) to be updated with server response 
		        type:      'post',        // 'get' or 'post', override for form's 'method' attribute 
		        dataType:  'json',        // 'xml', 'script', or 'json' (expected server response type)
		        success: function (responseText, statusText, xhr, $form){
		        	//alert('status: ' + statusText + '\n\nresponseText: \n' + responseText); 
				  	parent.location.reload(); 
					//关闭PickerBox
					parent.$.fancybox.close();
		        },  // post-submit callback 
		        error: function(XMLHttpRequest, textStatus, errorThrown){
					alert('Internal server error');
					//alert(XMLHttpRequest.responseText);
					//some stuff on failure
				}
		    }); 
		    
		    //If you want to use the default settings and have the UI blocked for all ajax requests, it's as easy as this:
		    $(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);
		    
		    //聚焦第一个输入框
			$("#project_name").focus();
			//为inputForm注册validate函数
			$("#myForm").validate();
		}); 
		 
	</script>
	
</head>
</head>

<body>
	
	<form id="myForm" action="${ctx}/project/${action}" method="post" class="form-horizontal">
		<input type="hidden" name="id" value="${project.id}"/>
		<fieldset>
			<legend><small>管理方案</small></legend>
			<div class="control-group">
				<label for="project_name" class="control-label">方案名称:</label>
				<div class="controls">
					<input type="text" id="project_name" name="name"  value="${project.name}" class="input-large required" minlength="3"/>
				</div>
			</div>	
			<div class="control-group">
				<label for="project_description" class="control-label">项目描述:</label>
				<div class="controls">
					<textarea id="project_description" name="description" class="input-large">${project.description}</textarea>
				</div>
			</div>	
			<div class="form-actions">
				<a class="btn btn-primary" href="javascript:$('#myForm').submit();">提交<i class="icon-ok"></i></a>
			</div>
		</fieldset>
	</form>

</body>
</html>
