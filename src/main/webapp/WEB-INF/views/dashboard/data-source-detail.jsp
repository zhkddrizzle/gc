<%@ page contentType="text/html;charset=UTF-8" %>
<%@ page import="org.apache.shiro.web.filter.authc.FormAuthenticationFilter"%>
<%@ page import="org.apache.shiro.authc.ExcessiveAttemptsException"%>
<%@ page import="org.apache.shiro.authc.IncorrectCredentialsException"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>

<script type="text/javascript">
	//jquery.uploadify
	$(function() {
	    $("#driverFile").uploadify({
	    	debug           : false,
	        height          : 30,
	        swf             : '${ctx}/static/uploadify/uploadify.swf',
	        uploader        : '${ctx}/data-source/ajax/upload-dirver-file/${projectId}',
	        width           : 120,
	        file_post_name  : 'driverFile',
	        onUploadSuccess : function(){
	        	$.ajax({
	    			url: "${ctx}/data-source/ajax/detail/${projectId}",
	    		    cache: true
	    		}).done(function( html ) {
	    		   $("#data-source-detail").html(html);
	    		});
	        }
	    });
	});
	
	//测试数据库连接
	function testDatabaseConnect(projectId){
		$.ajax({
			url: "${ctx}/data-source/ajax/test-db-connect/"+projectId,
		    cache: true
		}).done(function( data ) {
			var html='';
			if(data.result){
				html="<div class='alert alert-success'>"+
					"<button type='button' class='close' data-dismiss='alert'>×</button>"+
					"<strong>连接成功！</strong> "+
				"</div> ";
			}else{
				html="<div class='alert alert-error'>"+
					"<button type='button' class='close' data-dismiss='alert'>×</button>"+
					"<strong>连接失败！<br/>"+data.message+"</strong>"+
				"</div>";
			}
			
		   $("#testResult").html(html);
		   
		});
	}
	
	
</script>	
<div class="box-content">
	<table class="table">
		<tr>
			<td><i class="icon-cog"></i> <span class="blue"><b> 驱动名称</b></span></td>
			<td colspan="2"><code>${dataSource.driver}</code></td>
		</tr>
		<tr>
			<td><i class="icon-refresh"></i> <span class="blue"><b> 链接url</b></span></td>
			<td colspan="2"><code>${dataSource.url}</code></td>
		</tr>
		<tr>
			<td><i class="icon-user"></i> <span class="blue"><b> 用户名</b></span></td>
			<td colspan="2"><code>${dataSource.username}</code></td>
		</tr>
		<tr>
			<td><i class="icon-th"></i> <span class="blue"><b> 密码</b></span></td>
			<td colspan="2"><code>${dataSource.password}</code></td>
		</tr>
		<tr>
			<td><i class="icon-cog"></i> <span class="blue"><b> 驱动文件</b></span></td>
			<td><code>${dataSource.driverFileName}</code></td>
			<td width="140"><input data-no-uniform="true" type="file" name="driverFile" id="driverFile" /></td>
		</tr>
	</table>
	<div class="center">
		<a href="javascript:testDatabaseConnect(${projectId});" class="btn btn-large">测试连接</a>
		<div id="testResult"></div>
		
	</div>
</div>
