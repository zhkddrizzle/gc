<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="sitemesh" uri="http://www.opensymphony.com/sitemesh/decorator" %>  
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:set var="ctx" value="${pageContext.request.contextPath}" />

<html>
<head>
<title>微码代码生成器<sitemesh:title/></title>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<meta http-equiv="Cache-Control" content="no-store" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Expires" content="0" />
<!-- The styles -->
	<link id="bs-css" href="${ctx}/static/charisma-master/css/bootstrap-cerulean.css" rel="stylesheet">
	<style type="text/css">
	    body {
		    padding-bottom: 40px;
	    }
	    .sidebar-nav {
		    padding: 9px 0;
	    }
	    /* footer */
	    #footer {
			margin-top: 15px;
			padding: 15px 0px 0px 0px;
			font-size: 95%;
			text-align: center;
			/*border-top: 2px solid #cccccc;*/
	    }
		
	    #footer a {color: #999;}
	</style>
	<link href="${ctx}/static/charisma-master/css/bootstrap-responsive.css" rel="stylesheet" />
	<link href="${ctx}/static/charisma-master/css/charisma-app.css" rel="stylesheet" />
	<link href="${ctx}/static/charisma-master/css/jquery-ui-1.8.21.custom.css" rel="stylesheet" />
	<link href='${ctx}/static/charisma-master/css/fullcalendar.css' rel='stylesheet' />
	<link href='${ctx}/static/charisma-master/css/fullcalendar.print.css' rel='stylesheet'  media='print' />
	<link href='${ctx}/static/charisma-master/css/chosen.css' rel='stylesheet' />
	<link href='${ctx}/static/charisma-master/css/uniform.default.css' rel='stylesheet' />
	<link href='${ctx}/static/charisma-master/css/colorbox.css' rel='stylesheet' />
	<link href='${ctx}/static/charisma-master/css/jquery.cleditor.css' rel='stylesheet' />
	<link href='${ctx}/static/charisma-master/css/jquery.noty.css' rel='stylesheet' />
	<link href='${ctx}/static/charisma-master/css/noty_theme_default.css' rel='stylesheet' />
	<link href='${ctx}/static/charisma-master/css/elfinder.min.css' rel='stylesheet' />
	<link href='${ctx}/static/charisma-master/css/elfinder.theme.css' rel='stylesheet' />
	<link href='${ctx}/static/charisma-master/css/jquery.iphone.toggle.css' rel='stylesheet' />
	<link href='${ctx}/static/charisma-master/css/opa-icons.css' rel='stylesheet' />
	<link href='${ctx}/static/uploadify/uploadify.css' rel='stylesheet' />
	<link href="${ctx}/static/jquery-validation/1.11.1/validate.css" rel="stylesheet">

	<!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
	  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

	<!-- The fav icon -->
	<link rel="shortcut icon" href="${ctx}/static/charisma-master/img/favicon.ico" />

	<!-- jQuery -->
	<script src="${ctx}/static/charisma-master/js/jquery-1.7.2.min.js"></script>
	
	<!--
	<script src="${ctx}/static/jquery/jquery-1.9.1.min.js"></script>
	-->
	<!-- fancyBox -->
	<link rel="stylesheet" href="${ctx}/static/fancyBox/2.1.2/source/jquery.fancybox.css?v=2.1.0" type="text/css" media="screen" />
	<script type="text/javascript" src="${ctx}/static/fancyBox/2.1.2/source/jquery.fancybox.pack.js?v=2.1.0"></script>
	
	<!-- highcharts -->
	<script src="${ctx}/static/highcharts/3.0.5/js/highcharts.js"></script>
<sitemesh:head/>
</head>

<body>

	<%@ include file="/WEB-INF/layouts/header.jsp"%>
	
	<sitemesh:body/>
	
	<%@ include file="/WEB-INF/layouts/footer.jsp"%>
	
	<!-- external javascript
	================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->

	<!-- jQuery UI -->
	<script src="${ctx}/static/charisma-master/js/jquery-ui-1.8.21.custom.min.js"></script>
	<!-- jQuery Validation -->
	<script src="${ctx}/static/jquery-validation/1.11.1/jquery.validate.min.js" type="text/javascript"></script>
	<script src="${ctx}/static/jquery-validation/1.11.1/messages_bs_zh.js" type="text/javascript"></script>
	
	<!-- transition / effect library -->
	<script src="${ctx}/static/charisma-master/js/bootstrap-transition.js"></script>
	<!-- alert enhancer library -->
	<script src="${ctx}/static/charisma-master/js/bootstrap-alert.js"></script>
	<!-- modal / dialog library -->
	<script src="${ctx}/static/charisma-master/js/bootstrap-modal.js"></script>
	<!-- custom dropdown library -->
	<script src="${ctx}/static/charisma-master/js/bootstrap-dropdown.js"></script>
	<!-- scrolspy library -->
	<script src="${ctx}/static/charisma-master/js/bootstrap-scrollspy.js"></script>
	<!-- library for creating tabs -->
	<script src="${ctx}/static/charisma-master/js/bootstrap-tab.js"></script>
	<!-- library for advanced tooltip -->
	<script src="${ctx}/static/charisma-master/js/bootstrap-tooltip.js"></script>
	<!-- popover effect library -->
	<script src="${ctx}/static/charisma-master/js/bootstrap-popover.js"></script>
	<!-- button enhancer library -->
	<script src="${ctx}/static/charisma-master/js/bootstrap-button.js"></script>
	<!-- accordion library (optional, not used in demo) -->
	<script src="${ctx}/static/charisma-master/js/bootstrap-collapse.js"></script>
	<!-- carousel slideshow library (optional, not used in demo) -->
	<script src="${ctx}/static/charisma-master/js/bootstrap-carousel.js"></script>
	<!-- autocomplete library -->
	<script src="${ctx}/static/charisma-master/js/bootstrap-typeahead.js"></script>
	<!-- tour library -->
	<script src="${ctx}/static/charisma-master/js/bootstrap-tour.js"></script>
	<!-- library for cookie management-->
	<script src="${ctx}/static/charisma-master/js/jquery.cookie.js"></script>
	<!-- calander plugin -->
	<script src='${ctx}/static/charisma-master/js/fullcalendar.min.js'></script>
	<!-- data table plugin -->
	<script src='${ctx}/static/charisma-master/js/jquery.dataTables.min.js'></script>

	<!-- chart libraries start -->
	<script src="${ctx}/static/charisma-master/js/excanvas.js"></script>
	<script src="${ctx}/static/charisma-master/js/jquery.flot.min.js"></script>
	<script src="${ctx}/static/charisma-master/js/jquery.flot.pie.min.js"></script>
	<script src="${ctx}/static/charisma-master/js/jquery.flot.stack.js"></script>
	<script src="${ctx}/static/charisma-master/js/jquery.flot.resize.min.js"></script>
	<!-- chart libraries end -->

	<!-- select or dropdown enhancer -->
	<script src="${ctx}/static/charisma-master/js/jquery.chosen.min.js"></script>
	<!-- checkbox, radio, and file input styler -->
	<script src="${ctx}/static/charisma-master/js/jquery.uniform.min.js"></script>
	<!-- plugin for gallery image view -->
	<script src="${ctx}/static/charisma-master/js/jquery.colorbox.min.js"></script>
	<!-- rich text editor library -->
	<script src="${ctx}/static/charisma-master/js/jquery.cleditor.min.js"></script>
	<!-- notification plugin -->
	<script src="${ctx}/static/charisma-master/js/jquery.noty.js"></script>
	<!-- file manager library -->
	<script src="${ctx}/static/charisma-master/js/jquery.elfinder.min.js"></script>
	<script src="${ctx}/static/charisma-master/js/elfinder.zh_CN.js"></script>
	<!-- star rating plugin -->
	<script src="${ctx}/static/charisma-master/js/jquery.raty.min.js"></script>
	<!-- for iOS style toggle switch -->
	<script src="${ctx}/static/charisma-master/js/jquery.iphone.toggle.js"></script>
	<!-- autogrowing textarea plugin -->
	<script src="${ctx}/static/charisma-master/js/jquery.autogrow-textarea.js"></script>
	<!-- multiple file upload plugin -->
	<script src="${ctx}/static/uploadify/jquery.uploadify.min.js"></script>
	<!-- history.js for cross-browser state change on ajax -->
	<script src="${ctx}/static/charisma-master/js/jquery.history.js"></script>
	<!-- application script for Charisma demo -->
	<script src="${ctx}/static/charisma-master/js/charisma.js"></script>
	<!-- jquery blockUI -->
	<script type="text/javascript" src="${ctx}/static/jquery-blockUI/jquery.blockUI.js"></script>
	
	
	<script>
		$(document).ready(function() {
			//If you want to use the default settings and have the UI blocked for all ajax requests, it's as easy as this:
		    $(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);
		});
	</script>
	
</body>
</html>